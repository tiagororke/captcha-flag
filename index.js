"use strict";

// to add variables in .env to process.env
const dotenv = require("dotenv");
dotenv.config();

const express = require("express");
const app = express();
const httpServer = require("http").createServer(app);
const socket = require("socket.io")(httpServer);
const forwarded = require('forwarded-for');

const PORT = process.env.PORT || 3000;
httpServer.listen(PORT)

app.use(function (req, res, next) {
   let address = forwarded(req, req.headers);
   console.log("proxy ip:" , req.ip);
   console.log("real ip:" , address);
   next();
});

app.use(express.static(__dirname + "/public"));

socket.on("connection", (socket) => {

   let address = socket.conn.remoteAddress;
   console.log(" -> New connection from " + address);

   socket.emit("ping", "welcome" + address);

   socket.on("disconnect", () => {
      console.log(" -> Lost connection from " + address);
   });
});

